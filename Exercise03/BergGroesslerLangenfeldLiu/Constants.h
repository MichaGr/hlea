#pragma once

//max 4 GiB
const unsigned long max_size = 4ul * (1ul << 31ul);
// block size 64 KiB
const unsigned long block_size = 64ul * (1ul << 11ul);

const unsigned long max_file_size = 10ul * (1ul << 21ul);


//from there, it should be maximum 160 blocks per file.
const unsigned long blocks_per_node = 160;

//and from there, we can , with fixed number of blocks,
//allocate this number of Inodes

const unsigned long max_inodes = 362873;
//based on 160 blocks per inode
const unsigned long count_datablocks = 58059680;
