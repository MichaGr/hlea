#include "BogoFs.h"

#include "Fuse-impl.h"

#include <iostream>
#include <string>
#include <cstring>
#include <math.h>

Inode* BogoFs::root = nullptr;
std::bitset<count_datablocks>* BogoFs::used_blocks = nullptr;
std::bitset<max_inodes>* BogoFs::used_inodes = nullptr;

BogoFs::BogoFs ()
{}

//------------------------------------------------------------------------------
BogoFs::~BogoFs ()
{}

void* BogoFs::init (struct fuse_conn_info* conn)
{
    //TODO for persistence, restore from file?

    char* data = new char[max_size];

    //allocate freeblocks, freeinodes
    used_blocks = new std::bitset<count_datablocks>;
    used_inodes = new std::bitset<max_inodes>;

    used_inodes->reset();
    used_blocks->reset();

    //create root
    root = allocate_inode(data,0);
    strcpy(root->name,"/");
    root->is_file = false;
    root->st_gid = getgid();
    root->st_uid = getuid();
    root->size = 0;
    root->mode = 0666;
    root->prev = nullptr;
    root->sub = nullptr;
    root->next = nullptr;

    return root;
}

//------------------------------------------------------------------------------

void BogoFs::destroy (void* private_data)
{
    //reicht das? alles sollte ja in den 4Gib stehen?
    char* data = static_cast<char*>(private_data);

    //hier datei schreiben einfügen
    delete[] data;
    delete used_blocks;
    delete used_inodes;
}

//------------------------------------------------------------------------------

int BogoFs::chmod (const char* path,
                   mode_t mode)
{
    //Inode* root = static_cast<Inode*>(fuse_get_context()->private_data);

    Inode* node = find_node(std::string(path),root);

    if(node)
    {
        node->mode = mode;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::getattr (const char* path,
                     struct stat * stbuf)
{
    std::cout << "getattr: " << std::string(path) << std::endl;
    // Inode* root = static_cast<Inode*>(fuse_get_context()->private_data);

    Inode* node = find_node(std::string(path),root);

    if(node)
    {
        stbuf->st_uid = node->st_uid;
        stbuf->st_gid = node->st_gid;
        stbuf->st_atime = time(nullptr); // TODO
        stbuf->st_mtime = node->m_time;
        stbuf->st_ctime = node->c_time;
        stbuf->st_size = node->size;
        stbuf->st_mode = node->mode;

        if(strcmp(node->name,"/") == 0)
        {
            stbuf->st_mode = S_IFDIR;
            stbuf->st_nlink = 1;
        }
        else
        {
            if(node->is_file)
            {
                stbuf->st_mode = S_IFREG;
                stbuf->st_nlink = 1;
            }
            else
            {
                stbuf->st_mode = S_IFDIR;
                stbuf->st_nlink = 1;
            }
        }
    }
    else
    {
        std::cout << "No Node Found" << std::string(path) << std::endl;
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::readdir (const char *path,
                     void *buf,
                     fuse_fill_dir_t filler,
                     off_t offset,
                     struct fuse_file_info *fi)
{
    filler(buf,".",nullptr,0);
    filler(buf,"..",nullptr,0);

    //find dir

    Inode* current = find_node(path,root);

    //    int counter = 0;

    bool first = true;
    if(current)
    {
        if(first)
        {
            first = false;
            current = current->sub;
        }
        else {
            current = current->next;
        }
        //iterate over
        while(current)
        {
            //            std::cout << "ITERATING: " << counter << " " << std::string(current->name) << std::endl;
            filler(buf,current->name,nullptr,0);
            current = current->next;
        }
    }
    else
    {
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::chown (const char* path,
                   uid_t uid,
                   gid_t gid)
{
    Inode* node = find_node(path,root);

    if(node)
    {
        node->st_gid = gid;
        node->st_uid = uid;
    }
    else
    {
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::create (const char* path,
                    mode_t mode,
                    struct fuse_file_info* fi)
{
    //find parent

    std::string stdPath(path);
    std::string filename = BogoFs::get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - filename.size());
    if(stdPath.size() != filename.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - filename.size() - 1);
    }

    Inode* parent = find_node(prefix.c_str(),root);


    Inode* new_file = allocate_next_inode(fuse_get_context()->private_data);


    strcpy(new_file->name,filename.c_str());

    Inode* sub = parent->sub;

    if(parent->sub == nullptr)
    {
        parent->sub = new_file;
        new_file->prev = parent;
    }
    //first element exists
    else
    {
        Inode* next = sub->next;
        sub->next = new_file;
        new_file->next = next;
        if(next)
        {
            next->prev = new_file;
        }
        new_file->prev = sub;
    }

    new_file->sub = nullptr;
    new_file->st_gid = getgid();
    new_file->st_uid = getuid();
    new_file->size = 0;
    new_file->is_file = true;
    new_file->mode = mode;

    //allocate blocks, right now fixed to 160 per inode, allocate all of them
    allocate_datablocks(new_file,0);
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::link (const char* from,
                  const char* to)
{
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::mkdir (const char* path,
                   mode_t mode)
{

    std::string stdPath(path);
    std::string dirname = BogoFs::get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - dirname.size());
    if(stdPath.size() != dirname.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - dirname.size() - 1);
    }
    Inode* parent = find_node(prefix.c_str(),root);


    Inode* newdir = allocate_next_inode(fuse_get_context()->private_data);
    strcpy(newdir->name,dirname.c_str());

    Inode* sub = parent->sub;

    if(parent->sub == nullptr)
    {

        parent->sub = newdir;
        newdir->prev = parent;
    }
    //first element exists
    else
    {

        Inode* next = sub->next;
        sub->next = newdir;
        newdir->next = next;
        if(next)
        {
            next->prev = newdir;
        }
        newdir->prev = sub;
    }

    newdir->sub = nullptr;
    newdir->st_gid = getgid();
    newdir->st_uid = getuid();
    newdir->size = 0;
    newdir->is_file = false;
    newdir->mode = mode;

    return 0;


}

//------------------------------------------------------------------------------

int BogoFs::rmdir (const char* path)
{
    std::string stdPath(path);
    std::string dirname = get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - dirname.size());
    if(stdPath.size() != dirname.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - dirname.size() - 1);
    }

    Inode* parent = find_node(prefix.c_str());

    bool first = true;
    bool found = false;

    Inode* current = parent->sub;

    while(!found && current)
    {
        if(first)
        {
            first = false;
            if(current)
            {
                if(std::string(current->name) == dirname && !current->is_file)
                {

                    //dir found
                    //first entry

                    parent->sub = current->next;

                    if(current->next)
                    {
                        current->next->prev = parent;
                    }

                    //remove everything in dir
                    rm_directory_entries(current->sub);
                    //now remove dir itself

                    free_inode(current->position);
                    found = true;
                }
                else
                {
                    current = current->next;
                }
            }
        }
        else
        {
            if(current)
            {
                if(std::string(current->name) == dirname && !current->is_file)
                {

                    if(current->prev)
                    {

                        current->prev->next = current->next;
                    }
                    if(current->next)
                    {

                        current->next->prev = current->prev;
                    }

                    rm_directory_entries(current->sub);
                    free_inode(current->position);
                    found = true;
                }
                else
                {
                    current = current->next;
                }
            }
        }
    }

    if(!found)
    {
        return -ENOENT;
    }
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::rename(const char *path, const char *name)
{
    Inode* node = find_node(std::string(path));

    if(node)
    {
        strcpy(node->name,name);
    }
    else
    {
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::statfs (const char* path,
                    struct statvfs* stbuf)
{
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::truncate_file (Inode* inode, off_t size)
{
    if (!inode->is_file){
        return -1;
    }
    long size_increase = size - inode->size;

    if (size_increase == 0){
        return 0;
    }
    else if (size_increase < 0){
        //      remove enough blocks
        int blocks_to_remove = floor(abs(size_increase) / block_size);
        while (blocks_to_remove > 0){
            free_datablock(inode->data_indices->pop_back());
            blocks_to_remove--;
        }
        //        reset the size
        inode->size = size;
        return 0;
    }
    else if (size_increase > 0){
        int blocks_to_append = floor(size_increase / block_size);
        while (blocks_to_append > 0){
            //            the 0 is just for future compatibility, if we want to optimize the method
            unsigned int new_block_index = allocate_data_block(0);
            if (new_block_index < 0){
                //                just pass the possible error
                return new_block_index;
            }
            inode->data_indices->push_back(new_block_index);
            blocks_to_append--;
        }
        //        reset the size
        inode->size = size;
        return 0;
    }
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::truncate (const char* path,
                      off_t size)
{
    std::string stdPath(path);
    Inode* inode = find_node(stdPath);
    return truncate_file(inode, size);
}

//------------------------------------------------------------------------------

int BogoFs::unlink (const char* path)
{
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::utimens (const char* path,
                     const struct timespec ts[2])
{
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::write (const char* path,
                   const char* buf,
                   size_t size,
                   off_t offset,
                   struct fuse_file_info* fi)
{
    std::string stdPath(path);
    //  TODO should we do some checking here, e.g. if this is a file thats writable or does fuse do this before?

    Inode* inode = find_node(stdPath);

    //    this is the number of the block we start writing in
    unsigned long start_index = floor(offset / block_size);
    //    this is the offset inside the first write block
    unsigned long block_offset = offset % block_size;
    unsigned long end_index = floor(offset + size / block_size);

    off_t data_written = 0;
    unsigned int nof_blocks = end_index - start_index;

    //    first of all allocate enough data blocks for the write operation
    if (inode->size < offset + size){
        int return_val = truncate_file(inode, offset + size);
        if (return_val < 0){
            //            just pass the error
            return return_val;
        }
    }

    while (nof_blocks >= 0)
    {
        //    convention is: the first entry in the data_indices vector stands for the position of the first block and so on
        unsigned long block_start = inode->data_indices->indices[end_index - nof_blocks];

        block_start = max_inodes * sizeof(Inode) + block_size * inode->data_indices->indices[end_index - nof_blocks];

        //      the block offset is only relevant for the first block write, afterwards it will stay 0
        unsigned long start_pos = block_start + block_offset;
        //        either write the first or middle blocks of the data; or write the last block of the data
        size_t copy_amount = std::min(block_size - block_offset, block_offset + size - data_written);
        char* data = static_cast<char*>(fuse_get_context()->private_data);
        memcpy( &data[start_pos], &buf[data_written], copy_amount);

        //      prepare the next iteration:
        data_written += copy_amount;
        block_offset = 0;
        --nof_blocks;
    }
    return data_written;
}

//------------------------------------------------------------------------------

int BogoFs::open (const char *path,
                  struct fuse_file_info *fi)
{

    Inode* node = find_node(path);

    if(!node)
    {
        return -ENOENT;
    }

    node->open_count++;
    if(node->fh != 0)
    {
        fi->fh = node->fh;
    }
    else
    {
        node->fh = get_fh();
        fi->fh = node->fh;
    }

    //TODO , check user permissions

    return 0;
}

//------------------------------------------------------------------------------

//does only the last release call count? if so, no need for decrementing

int BogoFs::release(const char *path,
                    struct fuse_file_info *fi)
{
    Inode* node = find_node(path);

    if(!node)
    {
        return -ENOENT;
    }

    node->open_count--;

    if(node->open_count == 0)
    {
        node->fh = 0;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::read (const char *path,
                  char *buf,
                  size_t size,
                  off_t offset,
                  struct fuse_file_info *fi)
{
    std::string stdPath(path);
    //  TODO should we do some checking here, e.g. if this is a file thats writable or does fuse do this before?

    Inode* inode = find_node(stdPath);

    //    this is the number of the block we start reading in
    unsigned long start_index = floor(offset / block_size);
    //    this is the offset inside the first write block
    unsigned long block_offset = offset % block_size;
    unsigned long end_index = floor(offset + size / block_size);

    off_t data_read = 0;
    unsigned int nof_blocks = end_index - start_index;

    while (nof_blocks >= 0)
    {
        unsigned long block_start = inode->data_indices->indices[end_index - nof_blocks];

        block_start = max_inodes * sizeof(Inode) + block_size * inode->data_indices->indices[end_index - nof_blocks];

        unsigned long start_pos = block_start + block_offset;
        size_t copy_amount = std::min(block_size - block_offset, block_offset + size - data_read);
        char* data = static_cast<char*>(fuse_get_context()->private_data);
        std::memcpy(&buf[data_read], &data[start_pos], copy_amount);

        //      prepare the next iteration:
        data_read += copy_amount;
        block_offset = 0;
        --nof_blocks;
    }
    return data_read;
}

//------------------------------------------------------------------------------

int BogoFs::access(const char *path, int permissions)
{
    Inode* node = find_node(path,root);

    if(node)
    {
        return 0;
    }
    else
    {
        return -ENOENT;
    }
}

//------------------------------------------------------------------------------
//-----HELPER-------------------------------------------------------------------
//------------------------------------------------------------------------------

Inode* BogoFs::find_node(const std::string& path, Inode* parent)
{
    if(!parent)
    {
        return nullptr;
    }

    if(path == "/")
    {
        return parent;
    }

    //manipulate path
    std::string::size_type next = path.find('/',1);
    bool lastEntry = false;
    std::string new_path = "";
    std::string suffix = "";

    //no next slash found, looking for data and in directory of data

    if(next == std::string::npos)
    {
        lastEntry = true;
    }
    if(lastEntry)
    {

        suffix = path.substr(1);
    }
    else
    {
        suffix = path.substr(1,next-1);
        new_path = path.substr(next);
    }

    //iterate over children
    std::string name = suffix;
    bool found = false;

    bool first = true;
    Inode* current = parent;
    while(!found)
    {
        if(first)
        {
            first = false;
            if(current->sub && std::string(current->sub->name) == name)
            {
                found = true;
                current = current->sub;
            }
            else if(current->sub)
            {
                current = current->sub;
            }
            else
            {
                break;
            }
        }

        else {
            if(current->next && std::string(current->next->name) == name)
            {
                found = true;
                current = current->next;
            }
            else if(current->next)
            {
                current = current->next;
            }
            else
            {
                break;
            }
        }
    }

    //File Inode found, return it
    if(current->is_file && found)
    {
        return current;
    }

    //Directory Inode found, go to sub Inode
    if(!current->is_file && found)
    {
        if(lastEntry)
        {
            return current;
        }
        else
        {
            return find_node(new_path,current);
        }
    }
    return nullptr;
}

//------------------------------------------------------------------------------

Inode* BogoFs::allocate_next_inode(void *private_data)
{
    //get next free inode from bitvector
    unsigned long next = 0;
    for(next = 0;next < max_inodes;++next)
    {
        if(!used_inodes->test(next))
        {
            break;
        }
    }

    std::cout << next << std::endl;

    //no free inodes
    if(next == max_inodes)
    {
        return nullptr;
    }

    return allocate_inode(private_data,next);
}

//------------------------------------------------------------------------------

Inode* BogoFs::allocate_inode(void* private_data, unsigned long offset)
{
    used_inodes->set(offset);
    char* data = static_cast<char*>(private_data);
    data += offset * sizeof(Inode);
    Inode* node = new(data) Inode;
    node->position = offset;
    return node;
}

//------------------------------------------------------------------------------

int BogoFs::allocate_data_block(unsigned long next_to)
{
    if (used_inodes->all()){
        //      there's no space left
        return -ENOENT;
    }
    //    TODO for now, no optimization, we just search for the first free block starting
    //      at the beginning, so next_to has no effect

    for (unsigned long i=0; i < max_inodes; ++i){
        if (!used_inodes->test(i)){
            //    TODO do we need locking for this?
            used_inodes->set(i);
            return i;
        }
    }

    return 0;
}

//------------------------------------------------------------------------------

Inode* BogoFs::get_inode(void *private_data, unsigned long offset)
{
    char* p = static_cast<char*>(private_data);
    p += offset * sizeof(Inode);

    return reinterpret_cast<Inode*>(p);
}

//------------------------------------------------------------------------------

std::string BogoFs::get_filename(const std::string &path)
{
    std::string::size_type next = path.rfind("/");
    return path.substr(next+1);
}

//------------------------------------------------------------------------------

void BogoFs::free_inode(unsigned long offset)
{
    used_inodes->reset(offset);
}

//------------------------------------------------------------------------------

void BogoFs::free_datablock(unsigned long offset)
{
    used_blocks->reset(offset);
}

//------------------------------------------------------------------------------

void BogoFs::rm_directory_entries(Inode *parent)
{
    if(parent == nullptr)
    {
        return;
    }

    Inode* current = parent;
    while (current)
    {
        if (!current->is_file)
        {
            if(current->sub)
            {
                rm_directory_entries(current->sub);
            }
        }

        unsigned long position = current->position;
        current = current->next;

        free_inode(position);
    }
}

//------------------------------------------------------------------------------

//unsigned long* BogoFs::allocate_datablock(void* private_data, unsigned long offset)
//{
//    std::cout << "allocate datablock" << std::endl;
//    if(used_blocks->test(offset))
//    {
//        return nullptr;
//    }
//    char* data = static_cast<char*>(private_data);
//    data += sizeof(Inode) * max_inodes + offset * block_size;
//    unsigned long* datablock = new(data) unsigned long;
//    used_blocks->set(offset);
//    return datablock;
//}

//------------------------------------------------------------------------------

//unsigned long* BogoFs::allocate_datablock_range(void *private_data,
//                                                unsigned long offset,
//                                                unsigned long range)
//{
//    //TODO do some check
//    std::cout << "allocate datablock in range" << std::endl;
//    char* data = static_cast<char*>(private_data);
//    data += sizeof(Inode) * max_inodes + offset * block_size;
//    unsigned long* datablock = new(data) unsigned long[range];
//    for(unsigned long i = offset; i < offset+ range;++i)
//    {
//        used_blocks->set(i);
//    }
//    return datablock;
//}

//------------------------------------------------------------------------------

//unsigned long* BogoFs::allocate_next_datablock(void* private_data)
//{
//    std::cout << "allocate next datablock" << std::endl;

//    //find free datablock

//    unsigned long free = 0;
//    while(used_blocks->test(free))
//    {
//        ++free;
//    }

//    if(free != count_datablocks)
//    {
//    return allocate_datablock(private_data,free);
//    }
//    return nullptr;

//}

//------------------------------------------------------------------------------

//unsigned long* BogoFs::allocate_next_datablock_range(void *private_data,
//                                                     unsigned long offset,
//                                                     unsigned long range)
//{
//    return nullptr;
//}
//------------------------------------------------------------------------------

void BogoFs::allocate_datablocks(Inode* node,
                                 unsigned long range)
{
    unsigned long i = range < node->data_indices->available() ? range : node->data_indices->available();
    unsigned long j = 0;
    while(i > 0 && j < count_datablocks)
    {
        if(!used_blocks->test(j))
        {
            node->data_indices->push_back(j);
            --i;
        }
        ++j;
    }
}

//------------------------------------------------------------------------------

unsigned long BogoFs::get_fh()
{
    static unsigned long fh = 1;
    return fh++;
}

//------------------------------------------------------------------------------
