#include "Inode.h"
#include "Constants.h"

Inode::DataArray::DataArray(unsigned long count_indices)
    : indices(new unsigned long(count_indices)),
      used_indices(0),
      max_size(count_indices)
{}

//------------------------------------------------------------------------------

void Inode::DataArray::push_back(unsigned long index)
{
    indices[used_indices++] = index;
}

//------------------------------------------------------------------------------

unsigned long Inode::DataArray::pop_back()
{
    if(used_indices >0)
    {
        --used_indices;
        return indices[used_indices];
    }
    return -1;
}

//------------------------------------------------------------------------------

Inode::Inode()
    : is_file(true),
      next(nullptr),
      prev(nullptr),
      sub(nullptr),
      name(new char[256]),
      m_time(0),
      c_time(0),
      size(0),
      mode(0),
      position(0),
      fh(0),
      open_count(0),
      data_indices(new DataArray(blocks_per_node))
{}

//------------------------------------------------------------------------------

Inode::~Inode()
{
    delete[] name;
    delete[] data_indices;
}
