#pragma once

#include <string>
#include <vector>
#include "Constants.h"
struct Inode
{
    struct DataArray
    {
        DataArray(unsigned long count_indices = blocks_per_node);
        unsigned long* indices;
        unsigned long used_indices;
        unsigned long max_size;

        void push_back(unsigned long index);
        unsigned long pop_back();
        inline unsigned long size() {return used_indices;}
        inline unsigned long available() {return max_size - used_indices;}

//        unsigned long operator[] (int index) const {return indices[index];}
//        unsigned long& operator[] (int index) {return indices[index];}
        inline unsigned long at(int index) {return indices[index];}
    };

    Inode();
    ~Inode();
    bool is_file;


    //die muessen auch alle durch indizes im Inode array ersetzt werden
    Inode* next;
    Inode* prev; //neu, wenn die erste datei, dann zeigt es auf den oberordner
    Inode* sub; //Nur genutzt, wenn is_file == false, zeigt auf erstes element im ordner
    char* name;
    uid_t st_uid;
    gid_t st_gid;
    time_t m_time;
    time_t c_time;
    long size;
    //TODO permissions
    mode_t mode;
    unsigned long position;
    unsigned long fh; //used, if file is open, stores filehandle
    unsigned long open_count;

    //Hier wollten wir eigentlich die verschachtelte variante nehmen
    //um zu starten erstmal einfach einen vector
    DataArray* data_indices;
};
