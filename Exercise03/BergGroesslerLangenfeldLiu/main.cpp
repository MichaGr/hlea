#include "BogoFs.h"

int main(int argc, char** argv)
{
    BogoFs bogoFs;

    int status = bogoFs.run(argc,argv);

    return status;
}
