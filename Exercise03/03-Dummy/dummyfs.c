#define _POSIX_C_SOURCE 200809L
#define FUSE_USE_VERSION 29

#include <fuse.h>

#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define DEBUG 0

typedef struct metaData 
{
	char name[200];
	char path[200];
	size_t size;
	size_t max_size;
	char* buffer;
    uid_t st_uid; // User ID of owner
    gid_t st_gid; // Group ID of owner
    time_t st_atim;   /* Time of last access */
    time_t st_mtim;   /* Time of last modification */
    time_t st_ctim;   /* Time of last status change */
	int file_open;
	int file_created;
    mode_t modee;
} metaData;


static char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}


static
void*
dummyfs_init (struct fuse_conn_info *conn)
{
#if DEBUG
    printf("init\n");
#endif
	metaData* meta_data;
	meta_data = malloc(sizeof(metaData));
	// allocate buffer with size 5 MiB
	meta_data->buffer = calloc(5, (1 << 21));
    meta_data->st_uid = getuid();
    meta_data->st_gid = getgid();
	meta_data->max_size = 5 * 1 << 20;
    printf("Max size: %zu \n" , meta_data->max_size);
	// set size to 0 since there is no data in buffer yet
	meta_data->size = 0;
	meta_data->file_open = 0;
	meta_data->file_created = 0;
    meta_data->modee = 0666;
	// this will be the only allowed path to write/read/open/truncate etc
	strcpy(meta_data->name,"matrix.out");
	strcpy(meta_data->path,"/");
	return meta_data;
}


static
void
dummyfs_destroy (void *private_data)
{
#if DEBUG
    printf("destroy\n");
#endif
    metaData* data  = fuse_get_context()->private_data;
	// FIXME is this necessary?
	free(data->buffer);
	free(data);
}

static
int
dummyfs_chmod (const char* path, mode_t mode)
{
#if DEBUG
    printf("chmod\n");
#endif
    metaData* data  = fuse_get_context()->private_data;
    data->modee = mode;
	return 0;
}

static
int
dummyfs_chown (const char* path, uid_t uid, gid_t gid)
{
#if DEBUG
    printf("chown\n");
#endif
	return 0;
}

static
int
dummyfs_create (const char* path, mode_t mode, struct fuse_file_info* fi)
{
#if DEBUG
    printf("create\n");
#endif
	return 0;
}

static
int
dummyfs_getattr (const char* path, struct stat* stbuf)
{
#if DEBUG
    printf("getattr\n");
#endif
	 /*            
 		stat() struct for ls 
 			   dev_t     st_dev;        ID of device containing file
               ino_t     st_ino;          Inode number 
               mode_t    st_mode;         File type and mode
               nlink_t   st_nlink;        Number of hard links
               uid_t     st_uid;          User ID of owner
               gid_t     st_gid;          Group ID of owner
               dev_t     st_rdev;         Device ID (if special file)
               off_t     st_size;         Total size, in bytes 
               blksize_t st_blksize;      Block size for filesystem I/O 
               blkcnt_t  st_blocks;       Number of 512B blocks allocated 

               struct timespec st_atim;   Time of last access 
               struct timespec st_mtim;   Time of last modification 
               struct timespec st_ctim;   Time of last status change 
 */
    metaData* meta_data  = fuse_get_context()->private_data;
    stbuf->st_uid = meta_data->st_uid;
    stbuf->st_gid = meta_data->st_gid;
    stbuf->st_atime = meta_data->st_atim;
    stbuf->st_mtime = meta_data->st_mtim;
    stbuf->st_ctime = meta_data->st_ctim;
    stbuf->st_size = meta_data->size;
    stbuf->st_mode = meta_data->modee;
    
    if(strcmp(path,"/") == 0)
    {
        stbuf->st_mode = S_IFDIR;
        if(meta_data->file_created == 0){
            stbuf->st_nlink = 0;
        }
        else{
            stbuf->st_nlink = 1;
        }
        
    }
    else{
        stbuf->st_mode = S_IFREG;
        stbuf->st_nlink =1;
    }    
     
	return 0;
}

static
int
dummyfs_link (const char* from, const char* to)
{
#if DEBUG
    printf("link\n");
#endif
	return 0;
}

static
int
dummyfs_mkdir(const char* path, mode_t mode)
{
#if DEBUG
    printf("mkdir\n");
#endif
	return 0;
}

static
int
dummyfs_open (const char* path, struct fuse_file_info* fi)
{
#if DEBUG
    printf("open\n");
#endif
    metaData* data  = fuse_get_context()->private_data;

    if (strcmp(path, concat(data->path, data->name)) == 0) {
    	// we only have one file, so this is just 1 as file handle
    	fi->fh = 1;
    	data->file_open = 1;
    	data->file_created = 1;
    	// TODO only when we create it set the mtime
	    data->st_atim = time(NULL);
	    data->st_mtim = time(NULL);
	    data->st_ctim = time(NULL);
        data->modee = 0666;
    	return 0;
    }
	return -ENOENT;
}



//ich möchte die datei path lesen, hier ist nur /matrix.out als pfad erlaubt
//buf ist quasi mein return value, da schreib ich alles rein und zwar ab offset
//bis offset + size
//keine ahnung, was ich mit fileinfo mache?
//meine daten sind in fuse_get_context()->private_data

static
int
dummyfs_read (const char* path, char* buf, size_t size, off_t offset, struct fuse_file_info* fi)
{
#if DEBUG
    printf("read\n");
#endif
    //da sollten unsere daten liegen
    //fuse_get_context() ist statisch
    metaData* data  = fuse_get_context()->private_data;
    
    data->st_atim = time(NULL); /* Aktualisieren des letzten Zugriffes */

    //zunaechst lassen wir nur matrix.out zu, pfad stimmt noch nicht? aendert sich der pfad
    //je nach mountpunkt
    if (strcmp(path, concat(data->path, data->name)) == 0)
    {


        //wenn mein offset groesser ist, als unsere 5MiB, nicht cool, gib 0 zurueck
        if (offset >= (off_t)data->size)
        {
            return 0;
        }

        //falls wir vom offset aus mit der groesse rauslaufen, gib nur zurueck, was zwischen
        //offset und der 5MiB Grenze liegt
        if (offset + (off_t)size > (off_t)data->size)
        {
            memcpy(buf, data->buffer + offset, data->size - offset);
            return data->size - offset;
        }

        //im normalfall, offset + size < 5MiB gib einfach die daten von offset bis offset + size zurueck
        memcpy(buf, data->buffer + offset, size);
        return size;
    }

    //der dateiname hat nicht unserem matrix.out entsprochen, gib fehler zurueck
    return -ENOENT;
}

static
int
dummyfs_readdir (const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)
{
#if DEBUG
    printf("readdir\n");
#endif
	metaData* data  = fuse_get_context()->private_data;

	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);

	if ( strcmp( path, data->path ) == 0 )
	{
		if (data->file_created) {
			filler( buf, data->name , NULL, 0 );
		}
	}
	return 0;
}

static
int
dummyfs_rmdir (const char* path)
{
#if DEBUG
    printf("rmdir\n");
#endif
	return 0;
}

static
int
dummyfs_statfs (const char* path, struct statvfs* stbuf)
{
#if DEBUG
    printf("statfs\n");
#endif
	return 0;
}


static 
int 
truncate_file(off_t size){
#if DEBUG
    printf("truncate file\n");
#endif
	// only allow truncating
	metaData* data  = fuse_get_context()->private_data;

	if (size >= (off_t) data->size){
		if (data->size != 0) {
			return -1;
		}
	}
	data->size = (size_t) size;
	// FIXME check the math here! otherwise we overwrite some used memory
	memset(data->buffer + size, 0, data->max_size - size);
	return 0;
}

static
int
dummyfs_truncate (const char* path, off_t size)
{
#if DEBUG
    printf("truncate\n");
#endif
	metaData* data  = fuse_get_context()->private_data;

	// simply clip the maximally allowed size
	if (size > (off_t) data->max_size){
		size = (off_t) data->max_size;
	}
	if (size < (off_t) data->size){
		truncate_file(size);
	}
	data->size = (size_t) size;
	return 0;
}

static
int
dummyfs_unlink (const char* path)
{
#if DEBUG
    printf("unlink\n");
#endif
	metaData* data  = fuse_get_context()->private_data;

	if (!(truncate_file(0) == 0)) {
		return -EIO;
	}
	data->size = 0;
	data->file_created = 0;

	return 0;
}

static
int
dummyfs_utimens (const char* path, const struct timespec ts[2])
{
#if DEBUG
    printf("utimens\n");
#endif
	return 0;
}



//sollte aehnlich funktionieren, wie read, nur dass wir diesmal von
//buf nach data kopieren, oder?
static
int
dummyfs_write (const char* path, const char* buf, size_t size, off_t offset, struct fuse_file_info* fi)
{
#if DEBUG
    printf("write\n");
#endif
    metaData* data  = fuse_get_context()->private_data;
    
    data->st_mtim = time(NULL); /* Aktualisieren der letzten Modifikation */

    //zunaechst lassen wir nur matrix.out zu, pfad stimmt noch nicht? aendert sich der pfad
    //je nach mountpunkt
    if (strcmp(path, concat(data->path, data->name)) == 0)
    {
        off_t len = (off_t)data->size;

        //wenn mein offset groesser ist, als unsere 5MiB, nicht cool, gib 0 zurueck
        if (offset >= len)
        {
            return 0;
        }

        //falls wir vom offset aus mit der groesse rauslaufen, gib nur zurueck, was zwischen
        //offset und der 5MiB Grenze liegt
        if (offset + (off_t)size > len)
        {
            memcpy(data->buffer+offset, buf, len - offset);
            return len - offset;
        }

        //im normalfall, offset + size < 5MiB gib einfach die daten von offset bis offset + size zurueck
        memcpy(data->buffer + offset, buf, size);

        // TODO set the st_mtim in the data struct 
        // TODO set the new size in the data struct, if we don't just overwrite existing data!
        return size;
    }

    //der dateiname hat nicht unserem matrix.out entsprochen, gib fehler zurueck
    return -ENOENT;
}

struct fuse_operations dummyfs_oper = {
	.chmod    = dummyfs_chmod,
	.chown    = dummyfs_chown,
	.create   = dummyfs_create,
	.getattr  = dummyfs_getattr,
	.link     = dummyfs_link,
	.mkdir    = dummyfs_mkdir,
	.open     = dummyfs_open,
	.read     = dummyfs_read,
	.readdir  = dummyfs_readdir,
	.rmdir    = dummyfs_rmdir,
	.statfs   = dummyfs_statfs,
	.truncate = dummyfs_truncate,
	.unlink   = dummyfs_unlink,
	.utimens  = dummyfs_utimens,
	.write    = dummyfs_write,
	.init 	  = dummyfs_init,
	.destroy  = dummyfs_destroy,
};

int main (int argc, char* argv[])
{
	return fuse_main(argc, argv, &dummyfs_oper, NULL);
}
