#include "BogoFs.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <iostream>
#include <sys/ioctl.h>
#include <errno.h>

int main(int argc, char** argv)
{
    int fd = open(argv[1], O_RDWR, O_CREAT, S_IRWXU);
    if(fd == -1)
    {
        std::cout << "File could not be opened" << std::endl;
    }

//     TODO read the size of the blockdevice and set as max_size
    lseek(fd,max_size-1,SEEK_SET);
    write(fd,"",1);
    void* mappedData = mmap(nullptr, max_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);
    if(mappedData == MAP_FAILED)
    {
        std::cout << "MAPPING FAILED" << std::endl;
        std::cout << strerror(errno) << std::endl;
        return 1;
    }
    BogoFs bogoFs(mappedData);

    //create new argc,argv
    int status = bogoFs.run(argc-1,&argv[1]);

    return status;
}
