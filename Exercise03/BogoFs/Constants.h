#pragma once

const unsigned long kiB = 1ul << 11ul;
const unsigned long MiB = 1ul << 21ul;
const unsigned long GiB = 1ul << 31ul;

//max 1 GiB
const unsigned long max_size = 1ul * GiB;
// block size 64 KiB
const unsigned long block_size = 16ul * kiB;

const unsigned long inode_size = block_size;


//253.89 MiB
const unsigned long blocks_per_node = 16249ul;

const unsigned long max_file_size = blocks_per_node * block_size;

//and from there, we can , with fixed number of blocks,
//allocate this number of Inodes

// FIXME do we need a floor here?
const unsigned long max_nodes = max_size / block_size;

const unsigned long UNDEFINED_INODE = max_nodes+1ul;
