#include "BogoFs.h"

#include "Fuse-impl.h"

#include <iostream>
#include <string>
#include <cstring>
#include <math.h>
#include <sys/mman.h>
#include <boost/dynamic_bitset.hpp>
#include <queue>

Inode* BogoFs::root = nullptr;
std::bitset<max_nodes>* BogoFs::used_nodes = nullptr;
std::bitset<max_nodes>* BogoFs::locked_nodes = nullptr;
void* BogoFs::data = nullptr;

std::mutex BogoFs::mtx;

//------------------------------------------------------------------------------

BogoFs::BogoFs ()
{}

//------------------------------------------------------------------------------

BogoFs::BogoFs(void* mappedData)
{
    data = mappedData;
}

//------------------------------------------------------------------------------

BogoFs::~BogoFs ()
{}

//------------------------------------------------------------------------------

void* BogoFs::init (struct fuse_conn_info* conn)
{
    std::cout << "init" << std::endl;
    used_nodes = new std::bitset<max_nodes>;
    locked_nodes = new std::bitset<max_nodes>;
    locked_nodes->reset();

    Superblock* superblock = reinterpret_cast<Superblock*>(data);

    if(!(superblock->fsCreated == 'y'))
    {
        std::cout << "Fs not created" << std::endl;
        superblock->fsCreated = 'y';
        //        char* data = new char[max_size];

        //allocate freeblocks, freeinodes

        used_nodes->reset();

        //create root
        root = reinterpret_cast<Inode*>(reinterpret_cast<char*>(data) + sizeof(Superblock));
        root = allocate_inode(data,0);
        //        root->index = 0;
        used_nodes->set(0);
        strcpy(root->name,"/");
        root->is_file = false;
        root->st_gid = getgid();
        root->st_uid = getuid();
        root->size = 0;
        root->mode = 0666;
        root->prev = UNDEFINED_INODE;
        root->sub = UNDEFINED_INODE;
        root->next = UNDEFINED_INODE;
    }
    else
    {
        std::cout << "Fs already created" << std::endl;
        root = reinterpret_cast<Inode*>(reinterpret_cast<char*>(data) + sizeof(Superblock));
        restore_used_inodes_and_blocks();
    }

    return root;
}


//------------------------------------------------------------------------------

void BogoFs::destroy (void* private_data)
{
    //reicht das? alles sollte ja in den 4Gib stehen?
    std::cout << "DESTROY" << std::endl;

    if(msync(data,max_size,MS_SYNC) == -1)
    {
        std::cout << "MSync failed" << std::endl;
    }
    munmap(data,max_size);

    delete used_nodes;
}

//------------------------------------------------------------------------------

int BogoFs::chmod (const char* path,
                   mode_t mode)
{
    //Inode* root = static_cast<Inode*>(fuse_get_context()->private_data);

    Inode* node = find_node(std::string(path),root);

    if(node)
    {
        node->mode = mode;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::getattr (const char* path,
                     struct stat * stbuf)
{
    std::cout << "getattr: " << std::string(path) << std::endl;
    // Inode* root = static_cast<Inode*>(fuse_get_context()->private_data);

    Inode* node = find_node(std::string(path),root);

    if(node)
    {
        stbuf->st_uid = node->st_uid;
        stbuf->st_gid = node->st_gid;
        stbuf->st_atime = time(0);
        stbuf->st_mtime = node->m_time;
        stbuf->st_ctime = node->c_time;
        stbuf->st_size = node->size;
        stbuf->st_mode = node->mode;

        if(strcmp(node->name,"/") == 0)
        {
            stbuf->st_mode = S_IFDIR;
            stbuf->st_nlink = 1;
        }
        else
        {
            if(node->is_file)
            {
                stbuf->st_mode = S_IFREG;
                stbuf->st_nlink = 1;
            }
            else
            {
                stbuf->st_mode = S_IFDIR;
                stbuf->st_nlink = 1;
            }
        }
    }
    else
    {
        std::cout << "No Node Found" << std::string(path) << std::endl;
        return -ENOENT;
    }

    std::cout << "getattr end" << std::endl;
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::readdir (const char *path,
                     void *buf,
                     fuse_fill_dir_t filler,
                     off_t offset,
                     struct fuse_file_info *fi)
{
    std::cout << "readdir: " << std::string(path) << std::endl;
    filler(buf,".",nullptr,0);
    filler(buf,"..",nullptr,0);

    //find dir

    Inode* current = find_node(path,root);

    //    int counter = 0;

    bool first = true;
    if(current)
    {
        if(first)
        {
            first = false;
            current = get_inode_from_index(current->sub);
        }
        else {
            current = get_inode_from_index(current->next);
        }
        //iterate over
        while(current)
        {
            //            std::cout << "ITERATING: " << counter << " " << std::string(current->name) << std::endl;
            filler(buf,current->name,nullptr,0);
            current = get_inode_from_index(current->next);
        }
    }
    else
    {
        return -ENOENT;
    }

    std::cout << "readdir end" << std::endl;

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::chown (const char* path,
                   uid_t uid,
                   gid_t gid)
{
    Inode* node = find_node(path,root);

    if(node)
    {
        node->st_gid = gid;
        node->st_uid = uid;
    }
    else
    {
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::create (const char* path,
                    mode_t mode,
                    struct fuse_file_info* fi)
{
    std::cout << "create: " << std::string(path) << std::endl;
    //find parent

    std::string stdPath(path);
    std::string filename = BogoFs::get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - filename.size());
    if(stdPath.size() != filename.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - filename.size() - 1);
    }

    Inode* parent = find_node(prefix.c_str(),root);


    Inode* new_file = allocate_next_inode(fuse_get_context()->private_data);


    strcpy(new_file->name,filename.c_str());

    Inode* sub = get_inode_from_index(parent->sub);

    if(sub == nullptr)
    {
        parent->sub = new_file->index;
        new_file->prev = parent->index;
    }
    //first element exists
    else
    {
        Inode* next = get_inode_from_index(sub->next);
        sub->next = new_file->index;
        if(next)
        {
            new_file->next = next->index;
            next->prev = new_file->index;
        }
        new_file->prev = sub->index;
    }

    new_file->sub = UNDEFINED_INODE;
    new_file->st_gid = getgid();
    new_file->st_uid = getuid();
    new_file->size = 0ul;
    new_file->is_file = true;
    new_file->mode = mode;

    //allocate blocks, right now fixed to 160 per inode, allocate all of them
//    FIXME don't use this function
//    TODO check if we need to allocate anything here
//    allocate_datablocks(new_file,0);
    std::cout << "create end" << std::endl;
    mtx.unlock();
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::link (const char* from,
                  const char* to)
{
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::mkdir (const char* path,
                   mode_t mode)
{
    mtx.lock();
    std::string stdPath(path);
    std::string dirname = BogoFs::get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - dirname.size());
    if(stdPath.size() != dirname.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - dirname.size() - 1);
    }
    Inode* parent = find_node(prefix.c_str(),root);


    Inode* newdir = allocate_next_inode(fuse_get_context()->private_data);
    strcpy(newdir->name,dirname.c_str());

    Inode* sub = get_inode_from_index(parent->sub);

    if(sub == nullptr)
    {

        parent->sub = newdir->index;
        newdir->prev = parent->index;
    }
    //first element exists
    else
    {

        Inode* next = get_inode_from_index(sub->next);
        sub->next = newdir->index;
        if(next)
        {
            newdir->next = next->index;
            next->prev = newdir->index;
        }
        newdir->prev = sub->index;
    }

    newdir->sub = UNDEFINED_INODE;
    newdir->st_gid = getgid();
    newdir->st_uid = getuid();
    newdir->size = 0;
    newdir->is_file = false;
    newdir->mode = mode;
    mtx.unlock();
    return 0;


}

//------------------------------------------------------------------------------

int BogoFs::rmdir (const char* path)
{
    mtx.lock();
    std::string stdPath(path);
    std::string dirname = get_filename(stdPath);
    std::string prefix =  stdPath.substr(0,stdPath.size() - dirname.size());
    if(stdPath.size() != dirname.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - dirname.size() - 1);
    }

    Inode* parent = find_node(prefix.c_str());
    bool first = true;
    bool found = false;

    Inode* current = get_inode_from_index(parent->sub);

    while(!found && current)
    {
        if(first)
        {
            first = false;
            if(current)
            {
                if(std::string(current->name) == dirname && !current->is_file)
                {

                    //dir found
                    //first entry

                    parent->sub = current->next;

                    if(get_inode_from_index(current->next))
                    {
                        get_inode_from_index(current->next)->prev = parent->index;
                    }

                    //remove everything in dir
                    rm_directory_entries(get_inode_from_index(current->sub));
                    //now remove dir itself

                    free_inode(current->index);
                    found = true;
                }
                else
                {
                    current = get_inode_from_index(current->next);
                }
            }
        }
        else
        {
            if(current)
            {
                if(std::string(current->name) == dirname && !current->is_file)
                {

                    if(get_inode_from_index(current->prev))
                    {

                        get_inode_from_index(current->prev)->next = current->next;
                    }
                    if(get_inode_from_index(current->next))
                    {

                        get_inode_from_index(current->next)->prev = current->prev;
                    }

                    rm_directory_entries(get_inode_from_index(current->sub));
                    free_inode(current->index);
                    found = true;
                }
                else
                {
                    current = get_inode_from_index(current->next);
                }
            }
        }
    }

    if(!found)
    {
        mtx.unlock();
        return -ENOENT;
    }
    mtx.unlock();
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::rename(const char *path, const char *name)
{
    Inode* node = find_node(std::string(path));

    if(node)
    {
        strcpy(node->name,name);
    }
    else
    {
        return -ENOENT;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::statfs (const char* path,
                    struct statvfs* stbuf)
{
    stbuf->f_blocks = max_nodes;
    stbuf->f_bfree = used_nodes->count();
    stbuf->f_bsize = block_size;
    stbuf->f_namemax = 50;
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::truncate_file (Inode* inode, off_t size)
{
    if (!inode->is_file){
        return -1;
    }
    long size_increase = size - inode->size;

    if (size_increase == 0){
        return 0;
    }
    else if (size_increase < 0){
        //      remove enough blocks
        int blocks_to_remove = floor(abs(size_increase) / block_size);
        while (blocks_to_remove > 0){
            free_datablock(inode->data_indices.pop_back());
            blocks_to_remove--;
        }
        //        reset the size
        inode->size = size;
        return 0;
    }
    else if (size_increase > 0){
        int blocks_to_append = floor(size_increase / block_size);
        while (blocks_to_append > 0){
            //            the 0 is just for future compatibility, if we want to optimize the method
            unsigned int new_block_index = allocate_next_data_block(0);
            if (new_block_index < 0){
                //                just pass the possible error
                return new_block_index;
            }
            inode->data_indices.push_back(new_block_index);
            blocks_to_append--;
        }
        //        reset the size
        inode->size = size;
        return 0;
    }
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::truncate (const char* path,
                      off_t size)
{
    std::string stdPath(path);
    Inode* inode = find_node(stdPath);
    return truncate_file(inode, size);
}

//------------------------------------------------------------------------------

int BogoFs::unlink (const char* path)
{
    // as rmdir
    mtx.lock();
    std::string stdPath(path);

    std::string filename = get_filename(stdPath);

    std::string prefix =  stdPath.substr(0,stdPath.size() - filename.size());
    if(stdPath.size() != filename.size() + 1)
    {
        prefix = stdPath.substr(0,stdPath.size() - filename.size() - 1);
    }

    //parent dir
    Inode* parent = find_node(prefix.c_str());

    Inode* current = get_inode_from_index(parent->sub);

    bool found = false;
    bool first = true;

    while(!found)
    {
        if(current)
        {
            if(std::string(current->name) == filename && current->is_file)
            {
                found = true;

                if(first)
                {
                    parent->sub = current->next;

                    if(get_inode_from_index(current->next))
                    {
                        get_inode_from_index(current->next)->prev = parent->index;
                    }
                }
                else
                {
                    get_inode_from_index(current->prev)->next = current->next;

                    if(get_inode_from_index(current->next))
                    {
                        get_inode_from_index(current->next)->prev = current->prev;
                    }
                }
                free_inode(current->index);
                break;
            }
        }
        else
        {
            break;
        }

        current = get_inode_from_index(current->next);
        first = false;
    }

    if(!found)
    {
        mtx.unlock();
        return -ENOENT;
    }

    mtx.unlock();
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::utime (const char* path,
                   struct utimbuf* timbuf)
{
    Inode* inode = find_node(std::string(path),root);

    if(inode->c_time == 0)
    {
        inode->c_time  = timbuf->actime;
    }
    inode->m_time = timbuf->modtime;
    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::write (const char* path,
                   const char* buf,
                   size_t size,
                   off_t offset,
                   struct fuse_file_info* fi)
{
    std::string stdPath(path);
    //  TODO should we do some checking here, e.g. if this is a file thats writable or does fuse do this before?

    Inode* inode = find_node(stdPath);

    //    this is the number of the block we start writing in
    unsigned long start_index = offset / block_size;
    //    this is the offset inside the first write block
    unsigned long block_offset = offset % block_size;
//    FIXME somehow this results in the wrong end index!??
    unsigned long end_index = (offset + size) / block_size;

    //    first of all allocate enough data blocks for the write operation
    if (inode->size < offset + size){
        int return_val = truncate_file(inode, offset + size);
        if (return_val < 0){
            //            just pass the error
            return return_val;
        }
    }

    off_t data_written = 0;
    unsigned int blocks_to_process = end_index - start_index + 1;

    while (blocks_to_process > 0)
    {
        unsigned long block_index = inode->data_indices.indices[end_index - blocks_to_process + 1];

        char* block = get_data_block_from_index(block_index);

        char* write_pos = block + block_offset;

        // expr. 3 means there should be blocks read on the rhs of the current block
        // expr. 2 means that this is the last block that should be read
        size_t copy_amount = blocks_to_process == 1 ? size - data_written : block_size - block_offset;
        std::memcpy(write_pos, &buf[data_written], copy_amount);

        //      prepare the next iteration:
        data_written += copy_amount;
        // now we are not reading the first block anymore, so no offset per block
        block_offset = 0;
        --blocks_to_process;
    }
    return data_written;
}

//------------------------------------------------------------------------------

int BogoFs::open (const char *path,
                  struct fuse_file_info *fi)
{

    Inode* node = find_node(path);

    if(!node)
    {
        return -ENOENT;
    }

    node->open_count++;
    if(node->fh != 0)
    {
        fi->fh = node->fh;
    }
    else
    {
        node->fh = get_fh();
        fi->fh = node->fh;
    }

    //TODO , check user permissions

    return 0;
}

//------------------------------------------------------------------------------

//does only the last release call count? if so, no need for decrementing

int BogoFs::release(const char *path,
                    struct fuse_file_info *fi)
{
    Inode* node = find_node(path);

    if(!node)
    {
        return -ENOENT;
    }

    node->open_count--;

    if(node->open_count == 0)
    {
        node->fh = 0;
    }

    return 0;
}

//------------------------------------------------------------------------------

int BogoFs::read (const char *path,
                  char *buf,
                  size_t size,
                  off_t offset,
                  struct fuse_file_info *fi)
{
    std::string stdPath(path);
    //  TODO should we do some checking here, e.g. if this is a file thats writable or does fuse do this before?

    Inode* inode = find_node(stdPath);

    //    this is the number of the block we start reading in
    unsigned long start_index = offset / block_size;
    //    this is the offset inside the first read block
    unsigned long block_offset = offset % block_size;
    unsigned long end_index = (offset + size) / block_size;

    off_t data_read = 0;
    unsigned int blocks_to_process = end_index - start_index + 1;

    while (blocks_to_process > 0)
    {
        unsigned long block_index = inode->data_indices.indices[end_index - blocks_to_process + 1];

        char* block = get_data_block_from_index(block_index);

        char* read_pos = block + block_offset;

        // expr. 3 means there should be blocks read on the rhs of the current block
        // expr. 2 means that this is the last block that should be read
        size_t copy_amount = blocks_to_process == 1 ? size - data_read : block_size - block_offset;
        std::memcpy(&buf[data_read], read_pos, copy_amount);

        //      prepare the next iteration:
        data_read += copy_amount;
        // now we are not reading the first block anymore, so no offset per block
        block_offset = 0;
        --blocks_to_process;
    }
    return data_read;
}

//------------------------------------------------------------------------------

int BogoFs::access(const char *path, int permissions)
{
    Inode* node = find_node(path,root);

    if(node)
    {
        return 0;
    }
    else
    {
        return -ENOENT;
    }
}

//------------------------------------------------------------------------------

int BogoFs::fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
    //if datasync non zero, only data not meta data shall be flushed, TODO
    //right now only call msync on data


    if(datasync != 0)
    {
        Inode* node = find_node(std::string(path),root);
        if(node)
        {
            for(unsigned int i = 0; i <node->data_indices.size();++i)
            {
                if(msync(reinterpret_cast<void*>(node->data_indices.at(i)),
                         sizeof(char) * block_size,
                         MS_SYNC) == -1)
                {
                    return -ENOENT;
                }
            }
        }
        else
        {
            return -ENOENT;
        }
    }
    else
    {
        if(msync(data,max_size,MS_SYNC) == -1)
        {
            return -ENOENT;
        }
    }
    return 0;

}

//------------------------------------------------------------------------------
//-----HELPER-------------------------------------------------------------------
//------------------------------------------------------------------------------

Inode* BogoFs::find_node(const std::string& path, Inode* parent)
{

    std::cout << "find node" << std::endl;

    std::cout << path << std::endl;
    std::cout << parent->index << std::endl;
    std::cout << parent->sub << std::endl;
    std::cout << parent->next << std::endl;
    std::cout << parent->prev << std::endl;
    //    std::cout << "parent: " << std::string(parent->name) << std::endl;
    if(!parent)
    {
        return nullptr;
    }

    if(path == "/")
    {
        return parent;
    }

    //manipulate path
    std::string::size_type next = path.find('/',1);
    bool lastEntry = false;
    std::string new_path = "";
    std::string suffix = "";

    //no next slash found, looking for data and in directory of data

    if(next == std::string::npos)
    {
        lastEntry = true;
    }
    if(lastEntry)
    {

        suffix = path.substr(1);
    }
    else
    {
        suffix = path.substr(1,next-1);
        new_path = path.substr(next);
    }

    //iterate over children
    std::string name = suffix;
    bool found = false;

    bool first = true;
    Inode* current = parent;
    while(!found)
    {
        if(first)
        {
            first = false;
            if(get_inode_from_index(current->sub) && std::string(get_inode_from_index(current->sub)->name) == name)
            {
                found = true;
                current = get_inode_from_index(current->sub);
            }
            else if(get_inode_from_index(current->sub))
            {
                current = get_inode_from_index(current->sub);
            }
            else
            {
                break;
            }
        }

        else {
            if(get_inode_from_index(current->next) && std::string(get_inode_from_index(current->next)->name) == name)
            {
                found = true;
                current = get_inode_from_index(current->next);
            }
            else if(get_inode_from_index(current->next))
            {
                current = get_inode_from_index(current->next);
            }
            else
            {
                break;
            }
        }
    }

    //File Inode found, return it
    if(current->is_file && found)
    {
        return current;
    }

    //Directory Inode found, go to sub Inode if necessary
    if(!current->is_file && found)
    {
        if(lastEntry)
        {
            return current;
        }
        else
        {
            return find_node(new_path,current);
        }
    }
    return nullptr;
}

//------------------------------------------------------------------------------

Inode* BogoFs::allocate_next_inode(void *private_data)
{
    //get next free inode from bitvector
    unsigned long next = 0;
    for(next = 0;next < max_nodes;++next)
    {
        if(!used_nodes->test(next))
        {
            break;
        }
    }

    std::cout << next << std::endl;

    //no free inodes
    if(next == max_nodes)
    {
        return nullptr;
    }

    return allocate_inode(private_data,next);
}

//------------------------------------------------------------------------------

Inode* BogoFs::allocate_inode(void* private_data, unsigned long offset)
{
    used_nodes->set(offset);
    char* data = reinterpret_cast<char*>(root);
    data += offset * sizeof(Inode);
    Inode* node = new(data) Inode;
    node->index = offset;
    node->next = UNDEFINED_INODE;
    node->prev = UNDEFINED_INODE;
    node->sub = UNDEFINED_INODE;
    return node;
}

//------------------------------------------------------------------------------

int BogoFs::allocate_next_data_block(unsigned long next_to)
{
    if (used_nodes->all()){
        //      there's no space left
        return -ENOENT;
    }
    //    TODO for now, no optimization, we just search for the first free block starting
    //      at the beginning, so next_to has no effect

    for (unsigned long i=0; i < max_nodes; ++i){
        if (!used_nodes->test(i)){
            //    TODO do we need locking for this?
            used_nodes->set(i);
            return i;
        }
    }

    return 0;
}

//------------------------------------------------------------------------------

Inode* BogoFs::get_inode(void *private_data, unsigned long offset)
{
    char* p = reinterpret_cast<char*>(root);
    p += offset * sizeof(Inode);

    return reinterpret_cast<Inode*>(p);
}

//------------------------------------------------------------------------------

std::string BogoFs::get_filename(const std::string &path)
{
    std::string::size_type next = path.rfind("/");
    return path.substr(next+1);
}

//------------------------------------------------------------------------------

void BogoFs::free_inode(unsigned long offset)
{
    used_nodes->reset(offset);
}

//------------------------------------------------------------------------------

void BogoFs::free_datablock(unsigned long offset)
{
    used_nodes->reset(offset);
}

//------------------------------------------------------------------------------

void BogoFs::rm_directory_entries(Inode *parent)
{
    if(parent == nullptr)
    {
        return;
    }

    Inode* current = parent;
    while (current)
    {
        if (!current->is_file)
        {
            if(current->sub)
            {
                rm_directory_entries(get_inode_from_index(current->sub));
            }
        }

        unsigned long position = current->index;
        current = get_inode_from_index(current->next);

        free_inode(position);
    }
}

//------------------------------------------------------------------------------

unsigned long BogoFs::get_fh()
{
    static unsigned long fh = 1;
    return fh++;
}

//------------------------------------------------------------------------------

Inode* BogoFs::get_inode_from_index(unsigned long index)
{

    if(index == UNDEFINED_INODE)
    {
        return nullptr;
    }

    char* d = reinterpret_cast<char*>(root);
    Inode* address = reinterpret_cast<Inode*>(d + index * sizeof(Inode));


    return address;
}

//------------------------------------------------------------------------------


char* BogoFs::get_data_block_from_index(unsigned long index)
{

    if(index == UNDEFINED_INODE)
    {
        return nullptr;
    }

    char* d = reinterpret_cast<char*>(root);
    char* address = d + index * block_size;
    return address;
}

//------------------------------------------------------------------------------

void BogoFs::restore_used_inodes_and_blocks()
{
    //create queue starting from root node

    used_nodes->reset();
    std::queue<unsigned long> q;

    q.push(root->index);

    while(!q.empty())
    {
        Inode* tmp = get_inode_from_index(q.front());
        q.pop();

        if(tmp->next != UNDEFINED_INODE)
        {
            q.push(tmp->next);
        }

        if(tmp->sub != UNDEFINED_INODE)
        {
            q.push(tmp->sub);
        }

        //restore bitsets

        used_nodes->set(tmp->index);

        for(unsigned long i = 0; i < tmp->data_indices.size();++i)
        {
            used_nodes->set(tmp->data_indices.at(i));
        }
    }
}

//------------------------------------------------------------------------------

void BogoFs::lock_inode(unsigned long index)
{
    mtx.lock();
    locked_nodes->set(index);
    mtx.unlock();
}

//------------------------------------------------------------------------------

void BogoFs::unlock_inode(unsigned long index)
{
    mtx.lock();
    locked_nodes->reset(index);
    mtx.unlock();
}

//------------------------------------------------------------------------------

bool BogoFs::is_inode_locked(unsigned long index)
{
    return locked_nodes->test(index);
}

//------------------------------------------------------------------------------
