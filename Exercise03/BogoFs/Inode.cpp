#include "Inode.h"
#include "Constants.h"

Inode::DataArray::DataArray()
    : used_indices(0)
{
    for(unsigned int i = 0; i < blocks_per_node;++i)
    {
        indices[i] = 0ul;
    }
}

//------------------------------------------------------------------------------

void Inode::DataArray::push_back(unsigned long index)
{
    indices[used_indices++] = index;
}

//------------------------------------------------------------------------------

unsigned long Inode::DataArray::pop_back()
{
    if(used_indices >0)
    {
        --used_indices;
        return indices[used_indices];
    }
    return -1;
}

//------------------------------------------------------------------------------

Inode::Inode()
    : name(""),
      is_file(true),
      index(UNDEFINED_INODE),
      next(UNDEFINED_INODE),
      prev(UNDEFINED_INODE),
      sub(UNDEFINED_INODE),
      a_time(0),
      m_time(0),
      c_time(0),
      size(0),
      mode(0),
      fh(0),
      open_count(0),
      data_indices()
{}

//------------------------------------------------------------------------------

Inode::~Inode()
{}

//------------------------------------------------------------------------------
