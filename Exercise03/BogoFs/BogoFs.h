#pragma once

#include <bitset>
#include <mutex>

#include "Fuse.h"
#include "Fuse-impl.h"

#include "Constants.h"
#include "Inode.h"

class BogoFs : public Fusepp::Fuse<BogoFs>
{
public:
    BogoFs();
    BogoFs(void*);
    ~BogoFs();


    static void*    init(struct fuse_conn_info* conn);
    static void     destroy(void* private_data);
    static int      chmod (const char* path, mode_t mode);
    static int      getattr (const char *path, struct stat *stbuf);
    static int      rename(const char* path, const char* name);

    static int      readdir(const char *path, void *buf,
                       fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi);
    static int      chown (const char* path, uid_t uid, gid_t gid);
    static int      create(const char* path, mode_t mode, struct fuse_file_info* fi);
    static int      link (const char* from, const char* to);
    static int      mkdir (const char* path, mode_t mode);
    static int      rmdir(const char* path);
    static int      statfs(const char* path, struct statvfs* stbuf);
    static int      truncate(const char* path, off_t size);
    static int      unlink(const char* path);
    static int      utime(const char* path, struct utimbuf* timbuf);
    static int      write(const char* path, const char* buf, size_t size, off_t offset, struct fuse_file_info* fi);
    static int      access(const char* path, int);
    static int      fsync(const char* path,int, struct fuse_file_info* fi);

    static int      open(const char *path, struct fuse_file_info *fi);
    static int      release(const char* path, struct fuse_file_info* fi);

    static int      read(const char *path, char *buf, size_t size, off_t offset,
                    struct fuse_file_info *fi);

private:
           //if you already got a specific Inode, and from there, you want to find
           //some sort of child, use this
           static Inode*   find_node(const std::string& path, Inode* parent = root);
           //offset here means number of Inodes,
           //e.g. root -> offset = 0, first element in root offset = 1...
           static Inode*   allocate_inode(void* private_data, unsigned long offset);
           static Inode*   allocate_next_inode(void* private_data);

           static void     free_inode(unsigned long offset);
           static void     rm_directory_entries(Inode* parent);

           static Inode*   get_inode(void* private_data, unsigned long offset);

           static void     free_datablock(unsigned long offset);
           static int allocate_next_data_block(unsigned long next_to);
           static int      truncate_file(Inode* inode, off_t size);

           static std::string get_filename(const std::string& path);
           static unsigned long get_fh();

           static Inode* get_inode_from_index(unsigned long index);
           static char* get_data_block_from_index(unsigned long index);

           static void restore_used_inodes_and_blocks();
           static void lock_inode(unsigned long index);
           static void unlock_inode(unsigned long index);
           static bool is_inode_locked(unsigned long index);
private:
           struct Superblock
           {
               char fsCreated;
           };

           static std::bitset<max_nodes>* used_nodes;
           static std::bitset<max_nodes>* locked_nodes;

           static void* data;
           static Inode* root; //just for easier access, points to pos of first Inode of our allocated memory
           static std::mutex mtx;
};
